// https://docs.cypress.io/api/introduction/api.html

describe('Wzy Component', () => {
  it('Visits the app root url', () => {
    cy.server();
    cy.route('PUT', 'https://jsonplaceholder.typicode.com/posts/1', 'fixtures:home-title.json');

    cy.visit('/');
  });
});

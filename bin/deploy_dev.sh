#!/bin/bash
LOCAL_SERVER_PATH=$( printenv WZY_HOST )
LOCAL_SERVER_PORT=$( printenv WZY_PORT )
x-terminal-emulator -e "npx json-server --watch ${LOCAL_SERVER_PATH:-"db/db.json"} -p ${LOCAL_SERVER_PORT:-3000}"
x-terminal-emulator -e "npm run serve"

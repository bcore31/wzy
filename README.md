# wzy

> Vue.js plugin to make components editable and independant from their data.

## Dependencies and Plugins

- [ Vue.js ](https://vuejs.org/)
- [ Jest ](https://jestjs.io/)
- [ Cypress ](https://www.cypress.io/)

> Plugins

- [ babel ](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-babel)
- [ eslint ](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-eslint)
- [ jest ](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-unit-jest)
- [ cypress ](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-e2e-cypress)
- [ styleguide ](https://github.com/vue-styleguidist/vue-cli-plugin-styleguidist)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```
### Run the styleguide
```
npm run styleguide
```
### Build the styleguide
```
npm run styleguide:build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

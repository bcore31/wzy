module.exports = {
  components: 'src/components/**/[A-Z]*.vue',
  defaultExample: true,
  ribbon: {
    url: 'https://gitlab.com/bcore31/wzy',
  },
  version: '1.1.1',
  usageMode: 'expand',
  exampleMode: 'expand',
};

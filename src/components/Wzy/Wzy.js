import get from '@/microutils/dist/lang/get';

import WzyClient from '@/plugin/clients/WzyClient';
import Ressource from '@/plugin/models/Ressource';
import WzyInput from '@/components/WzyInput/WzyInput.vue';

/**
 * Wzy component.
 * Handles the manipulation of a state to ensure editablility via a client.
 *
 * @export default
 */
export default {
  name: 'wzy',
  components: {
    WzyInput,
  },
  props: {
    /**
     * data to pass to the wzy instance
     */
    wzyData: {
      type: Object,
      default: () => ({}),
    },
    clientType: {
      type: String,
    },
    clientOptions: {
      type: Object,
      default: () => ({}),
    },
  },
  data() {
    return {
      /**
       * the base data object
       */
      wzy: {},
      /**
       * is the object in edit mode
       */
      editing: false,
      client: {},
      /**
       * toggle the default editing mode
       * and data displaying
       */
      defaultMode: false,
    };
  },
  methods: {
    get,
    getWzyKey(key) {
      let data = {};
      try {
        data = this.get(this.wzy, key);
      } catch (e) {}
      return data;
    },
    updateDataFromInput(key, data, accessor) {
      return this.updateData(key, Object.assign({}, this.wzy[key].value.data, { [accessor || this.wzy[key].value.accessor]: data }, { path: this.wzy[key].value.path, accessor: this.wzy[key].value.accessor }));
    },
    /**
     * createData method.
     * Create a data in the object and ensures reactivity.
     *
     * @public
     *
     * @param {String} label - the name of the data
     * @param {Object|String|Number|Array} data - the data to set
     *
     * @return {Object|String|Number|Array}
     */
    async createData(label, data) {
      try {
        const ressource = new Ressource(data.path, data, data.accessor);
        this.$set(this.wzy, label, {
          value: ressource,
          editing: this.wzy[label] ? this.wzy[label].editing : false,
          loading: true,
        });
        const value = await this.client.createRessource(ressource);
        this.$set(this.wzy, label, {
          value,
          editing: this.wzy[label] ? this.wzy[label].editing : false,
          loading: false,
        });
        /**
         * wzy-create event.
         * this event is fired every time a key is created
         *
         * @event wzy-create
         * @type {object|array|number|string|boolean}
         */
        this.$emit('wzy-create', { key: label, value: this.wzy[label].value });
        return this.wzy[label];
      } catch (e) {
        console.error(e);
      }
      return null;
    },
    /**
     * updateData method.
     * Update a data in the object and ensures reactivity.
     *
     * @public
     *
     * @param {String} label - the name of the data
     * @param {Object|String|Number|Array} data - the data to set
     *
     * @return {Object|String|Number|Array}
     */
    async updateData(label, data) {
      try {
        this.wzy[label].loading = true;
        const ressource = new Ressource(this.wzy[label].value.path, data, this.wzy[label].value.accessor);
        const value = await this.client.updateRessource(ressource);
        if (
          typeof this.wzy[label] === 'undefined'
          || typeof this.wzy === 'object'
          || typeof this.wzy === 'function'
        ) {
          this.$set(this.wzy, label, {
            value,
            editing: this.wzy[label].editing,
            loading: false,
          });
        } else {
          this.wzy[label].value = new Ressource(this.wzy[label].value, path, data, this.wzy[label].value.accessor);
          this.wzy[label].loading = false;
        }
        /**
         * wzy-update event.
         * this event is fired every time a key is updated, namespace with the name of the
         * key after the :
         *
         * @event wzy-update
         * @type {object|array|number|string|boolean}
         */
        this.$emit('wzy-update', { key: label, value: this.wzy[label].value });
        return this.wzy[label];
      } catch (e) {
        console.error(e);
      }
      return null;
    },
    /**
     * syncDataWithProp method.
     * Sync the wzy data object with the prop
     */
    syncDataWithProp() {
      Object.keys(this.wzyData).forEach(key => this.createData(key, this.wzyData[key]));
    },
    /**
     * toggleEditMode method.
     * toggle editing data with param input or inverse.
     *
     * @public
     *
     * @param {Boolean} [value=!this.editing] - the editing value, or inverse of current
     *
     * @return {Boolean}
     */
    toggleEditMode(value = !this.editing, key) {
      const property = key ? this.wzy[key] : this;
      property.editing = value;
    },
  },
  watch: {
    wzyData() {
      this.syncDataWithProp();
    },
  },
  created() {
    this.client = new WzyClient(this.clientType, this.clientOptions);
  },
  mounted() {
    this.syncDataWithProp();
  },
};

export default {
  name: 'wzy-input',
  props: {
    value: {
      required: true,
    },
    updateData: {
      type: Function,
    },
  },
  methods: {},
};

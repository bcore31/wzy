import Vue from 'vue';
import App from './App.vue';

import WzyPlugin from '@/plugin/wzy';

Vue.use(WzyPlugin);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');

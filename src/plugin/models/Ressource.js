export default class Ressource {
  constructor(
    path,
    data,
    accessor,
    type = 'text',
  ) {
    this.path = path;
    this.data = data;
    this.accessor = accessor;
    this.type = type;
  }
}

import get from '@/microutils/dist/lang/get';

export default class RESTClient {
  constructor(options) {
    this.options = options;
    this.options.baseUrl = options.baseUrl || window.location.href;
  }

  async get(path, ressource) {
    const response = await fetch(`${this.options.baseUrl}/${path}`, {
      method: 'GET',
    });

    try {
      const clone = await response.clone();
      const json = await clone.json();
      return json;
    } catch (e) {
      throw e;
    }
  }

  async create(path, ressource) {
    const response = await fetch(`${this.options.baseUrl}/${path}`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(ressource.data),
      cors: true,
    });

    try {
      const clone = await response.clone();
      const json = await clone.json();
      return json;
    } catch (e) {
      throw e;
    }
  }

  async update(path, ressource) {
    const response = await fetch(`${this.options.baseUrl}/${path}`, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(ressource.data),
      cors: true,
    });

    try {
      const clone = await response.clone();
      const json = await clone.json();
      return json;
    } catch (e) {
      throw e;
    }
  }
}

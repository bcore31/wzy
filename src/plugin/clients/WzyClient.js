import Ressource from '@/plugin/models/Ressource';
import RESTClient from "@/plugin/clients/RESTClient"; // eslint-disable-line
import LocalClient from "@/plugin/clients/LocalClient"; // eslint-disable-line

const clients = {
  RESTClient,
  LocalClient,
};

export default class WzyClient {
  constructor(type = 'Local', options = {}) {
    this.type = type;
    this.options = options;
    this.client = {};
    this.setClient();
  }

  /**
   * setClient method.
   * set the client instance to use
   *
   * @public
   *
   * @param {String} [type = this.type] - the client constructor type
   * @param {Object} [options = this.clientOptions] - this client instance options
   *
   * @returns {Client}
   */
  setClient(type = this.type, options = this.options) {
    if (type) {
      const ClientConstructor = clients[`${type}Client`];
      if (!ClientConstructor) throw new Error(`client type ${type} is not defined`);
      this.client = new ClientConstructor(options);
    } else {
      this.client.baseData = this.options.baseData;
    }
    return this.client;
  }

  async getRessource(ressource) {
    let res = ressource;

    try {
      res = await this.client.get(ressource.path, ressource);
      res = new Ressource(ressource.path, res, ressource.accessor);
    } catch (e) {
      console.error(e);
      res = ressource;
    }

    return res;
  }

  async createRessource(ressource) {
    const json = await this.getRessource(ressource);
    return json;
  }

  async updateRessource(ressource) {
    let res = ressource;

    try {
      res = await this.client.update(ressource.path, ressource);
      res = new Ressource(ressource.path, res, ressource.accessor);
    } catch (e) {
      console.error(e);
      return res;
    }

    return res;
  }
}

import get from '@/microutils/dist/lang/get';

export default class LocalClient {
  constructor(options) {
    this.options = options;
  }

  async get(path, ressource) {
    try {
      return ressource.data;
    } catch (e) {
      throw e;
    }
  }

  async create(path, ressource) {
    try {
      return ressource.data;
    } catch (e) {
      throw e;
    }
  }

  async update(path, ressource) {
    try {
      return ressource.data;
    } catch (e) {
      throw e;
    }
  }
}

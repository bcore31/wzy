import WzyComponent from '@/components/Wzy/Wzy.vue';

export default {
  install(Vue, options) {
    Vue.component('wzy', WzyComponent);
  },
};
